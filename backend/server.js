import express from "express";
import morgan from "morgan";
import dotenv from "dotenv";
import bcrypt from "bcrypt";
import jwt from 'jsonwebtoken';
import cors from 'cors';

import connectDB from "./dbConfig&Model/db.js";
import Auth from "./middleware/auth.js";
import User from "./dbConfig&Model/usermodel.js";

connectDB();

const app = express();
app.use(express.json());
app.use(morgan('tiny'));
app.use(cors());

dotenv.config();

app.get('/',(req,res) => {
    res.json({ data : 'send'})
})

app.post('/register', (req, res) => {
    try {
        const { name , email , password } = req.body;
        bcrypt.hash(password, 10).then((hashPassword) =>{
            const user = new User({name, email, password: hashPassword});
            user.save();
            return res.status(200).json({ _id: user._id , name: user.name, email: user.email, password: user.password})
    });
    } catch (err) {
        return res.status(500).json({err});
    }

});

app.post('/login', (req, res) => {
    try {
        const { email , password } = req.body;
        User.findOne({email}).then((user) => {
            bcrypt.compare(password , user.password, (err, result) => {
                if (err || !result) return res.status(401).send(err || 'Invalid password');
                    const token = jwt.sign({ id: user._id, name: user.name}, process.env.JWT_SECRET , {expiresIn: '5s'});
                    const refreshToken = jwt.sign({ id: user._id, name: user.name}, process.env.JWT_REFRESH_SECRET);

                    const { password , ...restParams } = user._doc
                    return res.status(201).json({ user: restParams , token , refreshToken})
            })
        }) 
    }catch (err) {
        res.status(500).json({err});
    }
});

app.get('/refresh', Auth , (req,res) => {
    try {
        const { user } = req
        const token = jwt.sign({ id: user.id, name: user.name}, process.env.JWT_SECRET )  //{expiresIn:'10s'});
        return res.status(201).json(token);
    }catch (err) {
        res.status(500).json({err});
    }
});

app.get('/protect', Auth , (req,res) => {
try {
    return res.status(200).json({ message: "protected Route"});
} catch (err) {
    res.status(500).json({err});
}
})

const PORT = process.env.PORT || 5000

app.listen(PORT , () => {
    console.log(`Server running on port ${PORT}`);
});
