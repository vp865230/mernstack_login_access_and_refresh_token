import Jwt from "jsonwebtoken"
const Auth = async (req,res,next) => {
    try {
        const token = req.get('authorization')
        if (!token) return res.status(403).send("There is No Token");
        const jwtSecrate = req.originalUrl.includes('refresh') ? process.env.JWT_REFRESH_SECRET : process.env.JWT_SECRET
        Jwt.verify(token.split(' ')[1], jwtSecrate, (err , user) => {
            if ( err && err?.name === 'TokenExpiredError') return res.status(403).send("Token Expired")
            if (err) return res.status(403).send("Invalid Token");
            return res.send(user);
            req.user = user
            next();
        })
    }catch (err) {

    }
};

export default Auth;