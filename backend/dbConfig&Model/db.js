import mongoose from "mongoose";

const connectDB  = async () => {
    try { 
        const conn = await mongoose.connect( "mongodb://127.0.0.1:27017/user", {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
        console.log(`MongoDB Connected: ${conn.connection.host}`)
    }catch (err) {
        console.log(err)
    }
};

export default connectDB;